import {Job} from "app/job";
import {Injectable} from '@angular/core';
import {Http, RequestOptions, Headers, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/Rx';

const jobUrl = 'http://localhost:8082/ResourceServer/api/job';
const jobFetchUrl = 'http://localhost:8082/ResourceServer/api/job/fetch?query=';

@Injectable()
export class JobService {

  constructor(private http: Http) {
  }

  saveJob(jobObject: Job): Observable<Job[]> {
    let jobDetails = JSON.stringify(jobObject);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    let options = new RequestOptions({headers: headers});
    return this.http.post(jobUrl, jobDetails, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  findAllJob(): Observable<Job[]> {
    return this.http.get(jobUrl)
      .map(this.extractData)
      .catch(this.handleError);
  }
  
  searchJobs(jobObject: any): Observable<Job[]> {
    let query = this.getUrlQuery(jobObject);
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    let options = new RequestOptions({headers: headers});

    return this.http.get(jobFetchUrl + query, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  getUrlQuery(jobObject: any){
    let jobDetails = JSON.stringify(jobObject);
    let query = "company:*" + jobObject.keyword + "*";
    console.log(query);
    return query;
  }

  findAJob(id: number): Observable<Job> {
    return this.http.get(jobUrl + "/" + id)
      .map(this.extractData)
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    // console.error(errMsg);
    return Observable.throw(errMsg);
  }

  private extractData(res: any) {
    let body = res.json();
    // console.log(body);
    return body || {};
  }
}
