import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { PostjobComponent } from './postjob/postjob.component';
import { HeaderComponent } from './header/header.component';
import { FindjobComponent } from './findjob/findjob.component';
import { EditjobComponent } from './editjob/editjob.component';
import {routing} from "./app.routes";
import {JobService} from "./job.service";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import { ViewjobComponent } from './viewjob/viewjob.component';
import { SplitSentence } from './split-sentence';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    PostjobComponent,
    HeaderComponent,
    FindjobComponent,
    EditjobComponent,
    ViewjobComponent,
    SplitSentence
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserModule,
    NgbModule.forRoot(),
    ReactiveFormsModule,
    routing,
  ],
  providers: [JobService],
  bootstrap: [AppComponent]
})
export class AppModule { }
