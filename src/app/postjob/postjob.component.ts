import { JobService } from './../job.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { Job } from "app/job";

@Component({
  selector: 'app-postjob',
  templateUrl: './postjob.component.html',
  styleUrls: ['./postjob.component.css']
})
export class PostjobComponent implements OnInit {

  jopTypes = [
    { "id": 1, "value": 'Full-Time' },
    { "id": 2, "value": 'Part-Time' }
  ];
  industries = [
    { "id": 1, "value": 'IT' },
    { "id": 2, "value": 'Banking' }
  ];
  job: Job[];
  postJobForm: FormGroup;
  errorMessage: any;

  successMessage: string;
  alertType: string;
  constructor(private fb: FormBuilder, private jobService: JobService) {
    this.createForm();
  };

  createForm() {
    this.postJobForm = this.fb.group({
      'title': ['', Validators.required],
      'jobType': this.fb.group({
        'id': ['1', Validators.required],
      }),
      'industry': this.fb.group({
        'id': ['1', Validators.required],
      }),
      'userEmail': ['', Validators.required],
      'company': '',
      'location': this.fb.group({
        'addressOne': ['', Validators.required],
        'addressTwo': ['', Validators.required],
        'city': ['', Validators.required],
        'province': ['', Validators.required],
        'postalCode': ['', Validators.required],
        'country': this.fb.group({
          'id': ['1', Validators.required],
        }),
      }),
      'description': ['', Validators.required],
      'password': '',
      'confirmPassword': '',
    });
  }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.postJobForm.value);
    let rslt = this.jobService.saveJob(this.postJobForm.value)
      .subscribe(
      (job: Job[]) => {
        this.toast("Details submitted successfully for review!", "success");
      },
      error => this.toast(<any>error, "danger"));
  }

  toast(text: string, alertType: string = "info") {
    this.successMessage = text;
    this.alertType = alertType;
  }
}
