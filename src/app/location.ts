import { Country } from "app/country";

export class Location {
    constructor(public id: number, public addressOne: string, public addressTwo: string,
                public city: string, public province: string, public postalCode: string,
                public country: Country) {}
}
