import {JobService} from './../job.service';
import {Validators} from '@angular/forms';
import {FormGroup, FormBuilder} from '@angular/forms';
import {Component, OnInit} from '@angular/core';
import {Job} from "app/job";
@Component({
  selector: 'app-editjob',
  templateUrl: './editjob.component.html',
  styleUrls: ['./editjob.component.css']
})
export class EditjobComponent implements OnInit {

  editJobForm: FormGroup;
  jopTypes = [
    {"id": 1, "value": 'Full-Time'},
    {"id": 2, "value": 'Part-Time'}
  ];
  industries = [
    {"id": 1, "value": 'IT'},
    {"id": 2, "value": 'Bussiness'}
  ];
  educationLevels = [
    {"id": 1, "value": 'Bachelers'},
    {"id": 2, "value": 'Master'}
  ];
  careers = [
    {"id": 1, "value": 'Experienced'},
    {"id": 2, "value": 'Manager-Level'}
  ];
  job: Job[];
  errorMessage: any;

  constructor(private fb: FormBuilder, private jobService: JobService) {
    this.createForm();
  }

  createForm() {
    this.editJobForm = this.fb.group({
      'title': ['', Validators.required],
      'jobType': [this.jopTypes[0], Validators.required],
      'industry': ['', Validators.required],
      'companyEmail': ['', Validators.required],
      'company': '',
      'resume': '',
      'salary': '',
      'educationLevel': '',
      'careerLevel': '',
      'requirements': '',
      'responsibilities': '',
      'location': this.fb.group({
        'addressOne': ['', Validators.required],
        'addressTwo': ['', Validators.required],
        'city': ['', Validators.required],
        'province': ['', Validators.required],
        'postalCode': ['', Validators.required],
        'country': ['', Validators.required],
      }),
      'description': ['', Validators.required],
    });
  }

  onEdit() {
    let rslt = this.jobService.saveJob(this.editJobForm.value)
      .subscribe(
        (job: Job[]) => {
          this.toast("Success!");
        },
        error => this.toast(<any>error));
  }

  toast(text: string, duration: number = 3000, style: string = "") {

  }

  ngOnInit() {
  }

}
