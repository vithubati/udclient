import {EditjobComponent} from './editjob/editjob.component';
import {FindjobComponent} from './findjob/findjob.component';
import {SearchComponent} from './search/search.component';
import {PostjobComponent} from './postjob/postjob.component';
import {Routes, RouterModule} from '@angular/router';
import {ViewjobComponent} from "./viewjob/viewjob.component";


const APP_ROUTES: Routes = [
  {path: '', component: SearchComponent},
  {path: 'edit', component: EditjobComponent},
  {path: 'post-a-job', component: PostjobComponent},
  {path: 'findjobs', component: FindjobComponent},
  {path: 'viewjob/:id', component: ViewjobComponent},
];

export const routing = RouterModule.forRoot(APP_ROUTES);
