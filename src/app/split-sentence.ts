
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'splitSentence'
})
export class SplitSentence implements PipeTransform {
    list: any[];
    transform(value: string, [separator]): string {
        if (value) {
            let splits = value.split(separator);
            console.log(splits);
            if (splits.length > 1) {
                return splits.shift();
            } else {
                return '';
            }
        } else {
            return '';
        }

    }
}