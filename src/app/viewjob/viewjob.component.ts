import { SplitSentence } from './../split-sentence';
import { Component, OnInit } from '@angular/core';
import { Job } from "app/job";
import {JobService} from "../job.service";
import { ActivatedRoute } from "@angular/router";


@Component({
  selector: 'app-viewjob',
  templateUrl: './viewjob.component.html',
  styleUrls: ['./viewjob.component.css']
})

export class ViewjobComponent implements OnInit {
  job : Job;
  requirements: any[];
  responsibilities: any[];

  constructor(private jobService : JobService, private activatedRoute : ActivatedRoute) {
   }

  ngOnInit() {
    let id = +this.activatedRoute.snapshot.params['id'];
    this.onFind(id);
  }

  onFind(id : number){
    this.jobService.findAJob(id).subscribe(
      (job : Job) => { this.extractData(job)  },
      error => this.toast(<any> error)
    );    
  }
  extractData(job:Job){
    this.job = job;
    if(job.requirements){
      this.requirements = job.requirements.split('|');
    }if(job.responsibilities){
      this.responsibilities = job.responsibilities.split('|');
    }
  }

  toast(text: string, duration: number = 3000, style: string = "") {

  }

}
