import { Employer } from './employer';
import { Experience } from './experience';
import { Industry } from './industry';
import { Education } from './education';
import { JobType } from './job-type';
import { Location } from './location';
export class Job {
  constructor(public title:string, public jobType: JobType, public industry:Industry,
              public company:string, public description:string, public education:Education,
              public requirements:string, public responsibilities:string, public experience:Experience,
              public location:Location, public salary:string, public deadline:Date, public workHours:string,
              public employer: Employer, public createdDate:Date){}
}
